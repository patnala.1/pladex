# dlibrary 

First download the entire repo

Step 1 :

Install Nodemon globally in your computer :  npm install -g nodemon

Install Mongo Db compass and make sure you have a database named 'dlibrary' and a collection named 'bookapis' and store json data on the collection 

here is a sample of json data ...

{
bookname:"Living in the Light"
bookimglink:"https://cdn.asaha.com/assets/thumbs/9b8/9b88b7c17f496a61fc78505130af28..."
booklink:"https://www.pdfdrive.com/download.pdf?id=10172273&h=84f0f3490acb0a861c..."
type:"book"
author:"shakti gawain"
subject:"dbms"
course:"btech"
branch:"cse"
likes:10
shares:10
}

Step 2 : 

navigate to backend directory and run 'npm install' to install all the required packages 
navigate to frontend directory and run 'npm install' to install all the required packages 

Step 3:
Start the backend server by navigating to backend folder and type 'nodemon src/index' . This is start the backend server at port 8000

Start the frontend server by navigating to frontend folder and type 'npm start' . This is start the front end server .

Make sure Mongo Db compass is running and is connected .

Note : You dont have to write any extra code just install the packages and setup mongo db compass and its dababase and collections properly . Use same names for database and collections .
