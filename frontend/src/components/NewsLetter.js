import React, { useState } from "react";

function NewsLetter() {
  const [mail, setmail]=useState();
  return (
    <React.Fragment>
      <div className="container text-center">
        <div className="row my-3">
          <div className="col-md-2"></div>
          <div
            className="col-md-8 pt-5 pb-3"
            style={{ backgroundColor: "#f5f6f7", borderRadius: "10px" }}
          >
            <h5>
              <i class="fas fa-envelope" style={{ color: "#3ad6ab" }}></i>
              <strong> Newsletter</strong>
            </h5>
            <br />
            <p>
              <strong>
                {" "}
                Subscribe to get latest updates and notifications of new
                features and events at PLADEX
              </strong>
            </p>
            <div className="row mt-4 mb-3">
              <div className="col-md-3"></div>
              <div className="col-md-6">
                <form action="https://docs.google.com/forms/d/e/1FAIpQLSdGcfaJF8cxduf2dd8FjzJSBXhk6nLqHRdLH22a78cWkIqo-g/formResponse" method="POST">
                  <div className="form-group">
                    <input
                      type="email"
                      id="Email"
                      name="entry.881454754"
                      className="form-control"
                      placeholder="Enter your Mail ID"
                      value={mail}
                      onChange={e => setmail(e.target.value)}
                      style={{
                        borderRadius: "5px",
                        border: "none",
                        outline: "none",
                        padding: "8px 15px",
                        width: "75%",
                      }}
                    />
                    
                  </div>
                  <button
                      className="btn btn-primary"
                      type="submit"
                      style={{
                        border: "none",
                        outline: "none",
                        borderRadius: "5px",
                        width: "25%",
                      }}
                    >
                      Subscribe
                    </button>
                </form>
              </div>
              <div className="col-md-3"></div>
            </div>
          </div>
          <div className="col-md-2"></div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default NewsLetter;
