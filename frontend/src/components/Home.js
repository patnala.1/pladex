import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import Foot2 from "./foot2";
import Navnew from "./Navnew";
import UpcomingEventsWeb from "./UpcomingEventsWeb";
import EventTemp2 from "./EventTemp2";
import HomeSlider from "./HomeSlider";
import OurMission from "./OurMission";
import CollWeb from "./CollWeb";
import UpcomEve from "./UpcomEve";
import Count from "./Count";
import CollabMob from "./CollabMob";
import FeatMob from "./FeatMob";
import AddToCalendar from 'react-add-to-calendar';

function Home() {
  // const [prop1, setprop1] = useState(false);

  const currentUrl = window.location.href;
  var params = new URL(currentUrl).searchParams.get("state");
  const event= {
    title: 'Sample Event',
    description: 'This is the sample event provided as an example only',
    location: 'Portland, OR',
    startTime: '2016-09-16T20:15:00-04:00',
    endTime: '2016-09-16T21:45:00-04:00',
};
var gapi = window.gapi ; 
/* 
  Update with your own Client Id and Api key 
*/
var CLIENT_ID = "623615460174-undrui8pi574v6qqtra0vo35vr58olqr.apps.googleusercontent.com"
var API_KEY = "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"
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"]
var SCOPES = "https://www.googleapis.com/auth/calendar.events"

const handleClick = () => {
  gapi.load('client:auth2', () => {
    console.log('loaded client')

    gapi.client.init({
      apiKey: API_KEY,
      clientId: CLIENT_ID,
      discoveryDocs: DISCOVERY_DOCS,
      scope: SCOPES,
    })

    gapi.client.load('calendar', 'v3', () => console.log('bam!'))

    gapi.auth2.getAuthInstance().signIn()
    .then(() => {
      
      var event = {
        'summary': 'Awesome Event!',
        'location': '800 Howard St., San Francisco, CA 94103',
        'description': 'Really great refreshments',
        'start': {
          'dateTime': '2020-06-28T09:00:00-07:00',
          'timeZone': 'America/Los_Angeles'
        },
        'end': {
          'dateTime': '2020-06-28T17:00:00-07:00',
          'timeZone': 'America/Los_Angeles'
        },
        'recurrence': [
          'RRULE:FREQ=DAILY;COUNT=2'
        ],
        'attendees': [
          {'email': 'lpage@example.com'},
          {'email': 'sbrin@example.com'}
        ],
        'reminders': {
          'useDefault': false,
          'overrides': [
            {'method': 'email', 'minutes': 24 * 60},
            {'method': 'popup', 'minutes': 10}
          ]
        }
      }

      var request = gapi.client.calendar.events.insert({
        'calendarId': 'primary',
        'resource': event,
      })

      request.execute(event => {
        console.log(event)
        window.open(event.htmlLink)
      })
      

      /*
          Uncomment the following block to get events
      */
      /*
      // get events
      gapi.client.calendar.events.list({
        'calendarId': 'primary',
        'timeMin': (new Date()).toISOString(),
        'showDeleted': false,
        'singleEvents': true,
        'maxResults': 10,
        'orderBy': 'startTime'
      }).then(response => {
        const events = response.result.items
        console.log('EVENTS: ', events)
      })
      */
  

    })
  })
}

  useEffect(() => {
    if (
      params ==
      "3dbb4af3c5c4487908ea5f23315a2b578c09c5b20be208917b17e08a52c51e3f"
    ) {
      alert("Logged out successfully!");
    } else if (
      params ==
      "b54dc0f6b309929dd43ef88aa54958f854af3aa23e1744da5ded5887d79a8d0e"
    ) {
      alert("Unable to logout!");
    }
  }, []);

  const [loginauth, setloginauth] = useState(false);

  const loginhandler = () => {
    setloginauth(true);
  };

  const [display, setdisplay] = useState(false);

  useEffect(() => {
    if (window.innerWidth < 769) {
      setdisplay(true);
    }
  }, [display]);

  return (
    <>
      <Navnew logincheck={loginauth} loginhandler={loginhandler} />
      <HomeSlider />
      <OurMission />
      {display ? <FeatMob /> : <UpcomEve />}
      {display ? <EventTemp2 /> : <UpcomingEventsWeb />}
      {display ? <CollabMob /> : <CollWeb />}
      <Count />
      <Foot2 />
    </>
  );
}

export default Home;
