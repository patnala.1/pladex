import axios from "axios";
//import "./filter.css";
import Filter from "./filter (1)";
import ReactPaginate from "react-paginate";
import Dropdown from "react-bootstrap/Dropdown";
import React, { useEffect, useState } from "react";
import "./btechsubject.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Likes from "../images/book likes.svg";
import Share from "../images/share.svg";
import Save from "../images/book save.svg";
import Dots from "../images/bookdot.svg";
import Btechimg1 from "../images/btechsubjectimg1.svg";
import Btechimg2 from "../images/btechsubjectnotfoundimg.svg";
import { Link } from "react-router-dom";
import searchtag1 from "../images/booksearch1.svg";
import line from "../images/book searchline.svg";
import Comment from "../images/book comment.svg";
import filter from "lodash.filter";
import Svg from "./Svgimg";
// import Select from 'react-select-nested';

function Btechsubject() {
  const [book, setbook] = useState([]);
  const [searchdata, setsearchdata] = useState(book);
  const [search, setsearch] = useState("");
  const [searchby, setsearchby] = useState("Bookname");
  const [filtere, setfilter] = useState("");
  const [filtertxt, setfiltertxt] = useState({
    key: " ",
    value: " ",
  });
  const [page, setpage] = useState(0);
  const usersperpage = 18;
  const pagesvisited = page * usersperpage;
  const [active, setactive] = useState([
    { id: 1, state: true },
    { id: 2, state: false },
    { id: 3, state: false },
    { id: 4, state: false },
    { id: 5, state: false },
    { id: 6, state: false },
  ]);
  const toggleclass = (e) => {
    setactive(e);
  };
  const clicking = (id1, id2, id3, id4, id5) => {
    document.getElementById(id1).style.borderBottom = "2px solid #3ad6ab";
    document.getElementById(id2).style.borderBottom = "none";
    document.getElementById(id3).style.borderBottom = "none";
    document.getElementById(id4).style.borderBottom = "none";
    document.getElementById(id5).style.borderBottom = "none";
  };

  const Increaselikes = (book) => {
    book.Likes = book.Likes + 1;
  }

  const Displayusers = searchdata
    .slice(pagesvisited, pagesvisited + usersperpage)
    .map((book) => {
      return (
        <div
          className="card col-md-3 px-3 py-4 my-3 justify-content-center"
          id={book._id}
          style={{ minWidth: 400 }}
        >
          <div className="maincard h-100 ">
            <div className="card-body px-5 justify-content-center">
              <p
                className="card-title title"
                style={{ overflow: "hidden", height: 70 }}
              >
                {book.Bookname}
              </p>
              <div className="d-flex">
                {
                  <div className="my-5" style={{ marginRight: "20px" }}>
                    {
                    book.Bookimglink.length > 3 ? 
                    <img src={book.Bookimglink} width="100px" alt="img"></img>
                     : <Svg bookname={book.Bookname} author={book.Authors}/>
    }
                  </div>
                }
                <div>
                  {/* <img src={Dots} style={{width:'3px'}} alt="img"></img> </div> */}

                  {/* <div className="d-flex flex-row-reverse"></div> */}

                  <p
                    className="card-text mt-5"
                    style={{ overflow: "hidden", height: 20 }}
                  >
                    <span className="span1">Author : </span>
                    <span className="span2">{book.Authors}</span>
                  </p>
                  <p
                    className="card-text"
                    style={{ overflow: "hidden", height: 20 }}
                  >
                    <span className="span1">Subject : </span>
                    <span className="span2">{book.Subject}</span>
                  </p>
                  <p className="card-text">
                    <span className="span1">Branch : </span>
                    <span className="span2">{book.Branch}</span>
                  </p>

                  <div className="lower my-5" style={{ display: "flex" }}>
                    <span className="lowerimg1">
                      <img src={Share} className="mx-3" alt="img"></img>
                    </span>
                    <img
                      src={Likes}
                      onClick={Increaselikes(book)}
                      className="mx-1"
                      alt="img"
                      id="likeimg"
                    ></img>
                    <span>{book.Likes}</span>
                    <span className="lowerimg2 mx-3">
                      <img src={Save} alt="img"></img>
                    </span>
                    <span className="mx-3">
                      <img src={Comment} alt="img"></img>
                    </span>
                  </div>
                  <a href={book.Booklink} className="btn btn1">
                    Download
                  </a>
                </div>
              </div>
            </div>{" "}
          </div>
        </div>
      );
    });

  const pageCount = Math.ceil(searchdata.length / usersperpage);

  const changePage = ({ selected }) => {
    setpage(selected);
  };
  function subfilterFunction(e) {
    setsearchby(e.target.value);
    console.log(searchby);
    console.log("h0");
  }
  function subfilterFunction1(e) {
    setfilter(e.target.value);
    console.log(filtere);
    console.log("hi");
  }

  const handlefilter1 = (e) => {
    setfilter(e.target.value);
    console.log(filtere);
    const query = filtere;
    const filteredData = filter(book, (user) => {
      return containSearchf(user, query);
    });
    setsearchdata(filteredData);
  };

  const handleSearch = () => {
    const query = search;
    if (query === "") {
      setsearchdata(book);
    }
    const filteredData = filter(book, (user) => {
      if (searchby === "Bookname") {
        return containSearch(user, query);
      } else if (searchby === "Author") {
        return containSearch1(user, query);
      } else if (searchby === "Subject") {
        return containSearch2(user, query);
      } else {
        return containSearchf(user, query);
      }
    });
    setsearchdata(filteredData);
  // axios.get('/bookapi/:' + search).then((response) => {
  //     console.log(search);
  //     setsearchdata(response.data);
  //   });
  };
  const containSearch = ({ Bookname }, query) => {
    const quer = query.toLowerCase();
    const name = Bookname.toLowerCase();
    if (name.includes(quer)) {
      return true;
    }
    return false;
  };
  const containSearch1 = ({ Authors }, query) => {
    const quer = query.toLowerCase();
    if(Authors){
      const name2 = Authors.toLowerCase();
      if (name2.includes(quer)) {
        return true;
      }
      return false;
    }
    
  };

  const containSearch2 = ({ Subject }, query) => {
    const quer = query.toLowerCase();
    const name3 = Subject.toLowerCase();
    if (name3.includes(quer)) {
      return true;
    }
    return false;
  };

  const containSearchf = ({ ShortForm, Bookname, Authors, Subject }, query) => {
    const name4 = ShortForm;
    const name5= Bookname;
    const name6 = Authors;
    const name7 = Subject;
    const quer = query;
    if (name4.includes(searchby)) {
      if (name5.includes(quer)) {
        return true;
      }
      if (name6.includes(quer)) {
        return true;
      }
      if (name7.includes(quer)) {
        return true;
      }
      
    }
    
    return false;
  };

  
  function filterFunction() {
    var count = 0;
    var input, filter, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    for (i = 0; i < book.length; i++) {
      var txt = book[i].bookname;
      if (txt.toUpperCase().indexOf(filter) > -1) {
        document.getElementById(book[i]._id).style.display = "block";
        count++;
      } else {
        document.getElementById(book[i]._id).style.display = "none";
      }
    }
    if (count === 0) {
      document.getElementById("notfound").style.visibility = "visible";
      document.getElementById("bookrow").style.display = "none";
    } else {
      document.getElementById("notfound").style.visibility = "hidden";
      document.getElementById("bookrow").style.display = "block";
    }
  }

  const BookCard = ({ book }) => {
    <div
      className="card col-md-3 px-3 py-3 my-3 justify-content-center d-flex"
      id={book._id}
    >
      <div className="maincard h-100 ">
        <div className="card-body ">
          <p
            className="card-title title"
            style={{ overflow: "hidden", height: 75 }}
          >
            {book.Bookname}
          </p>
          <div className="d-flex">
            {
              <div className="my-5" style={{ marginRight: "20px" }}>
                <img src={book.Bookimg} width="100px" alt="img"></img>
              </div>
            }
            <div>
              {/* <img src={Dots} style={{width:'3px'}} alt="img"></img> </div> */}

              {/* <div className="d-flex flex-row-reverse"></div> */}

              <p className="card-text mt-5">
                <span className="span1">Author : </span>
                <span className="span2">{book.Authors}</span>
              </p>
              <p className="card-text">
                <span className="span1">Subject : </span>
                <span className="span2">{book.Subject}</span>
              </p>
              <p className="card-text">
                <span className="span1">Branch : </span>
                <span className="span2">{book.Branch}</span>
              </p>

              <div className="lower my-5" style={{ display: "flex" }}>
                <span className="lowerimg1">
                  <img src={Share} className="mx-3" alt="img"></img>
                </span>
                <img src={Likes} onClick={Increaselikes(book)} className="mx-1" alt="img" id="likeimg"></img>
                <span>{book.Likes}</span>
                <span className="lowerimg2 mx-3">
                  <img src={Save} alt="img"></img>
                </span>
                <span className="mx-3">
                  <img src={Comment} alt="img"></img>
                </span>
              </div>
              <a href={book.Booklink} className="btn btn1">
                Download
              </a>
            </div>
          </div>
        </div>{" "}
      </div>
    </div>;
  };
  // useEffect(()=>{
  //   fetch('/bookapi',{
  //     headers : {
  //       'Content-Type': 'application/json',
  //       'Accept': 'application/json'
  //      }

  //   })
  //     .then(response => response.json())
  //     .then(data => {setbook(data);console.log(data);});
  //  },[])

  const handleHome = () => {
    setsearchdata(book);
    clicking("btn1", "btn2", "btn3", "btn4", "btn5");
    // document.getElementById('btn1').style.boxShadow = " 0px 3px green";
  };
  const highestFirst = (array) => {
    return array.sort((a, b) => {
      return b.Likes - a.Likes;
    });
  };

  const handleTrend = () => {
    console.log('hello');
    clicking("btn2", "btn1", "btn3", "btn4", "btn5");
    //document.getElementById('btn2').style.boxShadow = " 0px 3px green";
    axios.get("/bookapi/sort").then((response) => {
      // console.log(response.data)
      setsearchdata(response.data);
    });
  };
  
  useEffect(() => {
    axios.get("/bookapi").then((response) => {
      // console.log(response.data)
      setbook(response.data);
       setsearchdata(response.data);
    });
  }, []);
  // console.log(filtertxt.key);
  // console.log(filtertxt.value);

  return (
    <div className="books" style={{ overflowX: "hidden" }}>
      {/* <button className="button btn btn-success mx-3" id="searchbtn" onClick={handlefilter1}>Search</button> */}

      <div className="booking">
        <div className="d-flex justify-content-between mx-5">
          <div className="listofbookstagline display-5 mt-5 ">
            There is a book for everyone
          </div>
          {/*<input type='button' onClick={()=>{setfiltertxt({key:"branch",value:"cse"})}} value='submit'/>*/}
          {/* <p>{filtertxt}</p> */}
          <img src={Btechimg1} className="img1" alt="img"></img>
        </div>

         <div className="input-group col-md-4 justicy-content-center mx-auto" style={{ width: '40%', minWidth: '419px', borderRadius: 5}} >
          <div className="input-group-prepend d-flex" >
  
          
            <select
              className="btn dropdown shadow-none pt-0"
              name="searchby"
              defaultValue="Bookname"
              onChange={subfilterFunction}
              style={{height: 35,width: 80, backgroundColor: 'white', border: '1px solid black', borderTopLeftRadius: 5, borderBottomLeftRadius: 5}}
            >
              <option value="Author">Author</option>
              <option value="Bookname">Bookname</option>
              <option value="Subject">Subject</option>
              <hr></hr>
              <optgroup label="Branch">
                <option value="CSE">CSE</option>
                <option value="MRE">MRE</option>
                <option value="NE">NE</option>
                <option value="PT">PT</option>
                <option value="PE">PE</option>
                <option value="NTE">NTE</option>
                <option value="EDE">EDE</option>
                <option value="EME">EME</option>
                <option value="EIE">EIE</option>
                <option value="EE">EE</option>
                <option value="MCE">MCE</option>
                <option value="EC">EC</option>
                <option value="EEE">EEE</option>
                <option value="ECE">ECE</option>
                <option value="EIEE">EIEE</option>
              </optgroup>
            </select>
            {/* <div> <Select
                  placeholder="Select fruit"
                  list={filters}
                  onSelectChange={(item)=>console.log('use your custom handler here', item)}
              /> </div>   */}
        
            {/* <img src={line} alt="img"></img>
            
            <i
              className="fa fa-search mx-2"
              style={{ fontSize: "11px", color: "black" }}
            ></i>
            </span> */}
            </div>
            <input
            type="text"
            onChange={(event) => setsearch(event.target.value)}
            placeholder="Search for the book"
            id="myInput"
            className="form-control text-center"
            style={{border: '1px solid black', borderRadiusTopRight: 5, borderRadiusTopLeft:5}}
            
          >
          </input>
          </div>
          <button
            className="button btn btn-success "
            id="searchbtn"
            onClick={handleSearch}
           
          >
            Search
          </button>
          
        
      </div>
      <br></br>
      <nav
        className="booksbar my-3" 
        style={{ backgroundColor: "#C9EDDC", height: "30px" }}
      >
        <div className="bardiv text-left justify-content-flex-start" style={{ paddingTop: "5px" , justifyContent:'flex-start' }}>
          <a className={
                  active[0].state
                    ? "mx-2 my-2 text-dark px-5 py-2 active text-decoration-none"
                    : "mx-2 my-2 text-dark px-5 py-2 notactive text-decoration-none"
                } id="btn1" onClick={handleHome}>
            HOME
          </a>
          <a className={
                  active[1].state
                    ? "mx-2 my-2 text-dark px-4 py-2 active text-decoration-none"
                    : "mx-2 my-2 text-dark px-4 py-2 notactive text-decoration-none"
                }  onClick={  handleTrend} id="btn2">
            TRENDING
          </a>
          <a className="btn barbtn3 px-4 " id="btn3">
            NOTES
          </a>
          <a className="btn barbtn3 px-4 " id="btn4">
            NOVELS
          </a>
          <a className="btn barbtn4 px-4 " id="btn5">
            SAVED
          </a>
        </div>
      </nav>
      {searchdata.length > 0 ? 
      <div className="users row justify-content-center">
       
        {Displayusers}
        <ReactPaginate
          previousLabel={"Previous"}
          nextLabel={"Next"}
          pageCount={pageCount}
          onPageChange={changePage}
          containerClassName={"paginationBttns"}
          previousLinkClassName={"previousBttn"}
          nextLinkClassName={"nextBttn"}
          disabledClassName={"pagiantionDisabled"}
          activeClassName={"paginationActive"}
          previousClassName={"previouspage"}
          nextClassName={"nextpage"}
          pageLinkClassName={"pageBttn"}
          pageClassName={"pageclassBttn"}
        />
      </div>
      :

      <div
        className="notfound text-center mt-5"
        id="notfound"
      >
        <img src={Btechimg2} alt="img"></img>
        <p className="p1">sorry we dont have what you are looking for</p>
        <p className="p3">
          please tap on request and fill the details of what you are looking for
        </p>
        <Link to="/request">
          <div className="my-5">
            <button className="btn px-5">Request</button>
          </div>
        </Link> 
      </div> }
    </div>
  );
}
export default Btechsubject;
