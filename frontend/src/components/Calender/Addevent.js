import React, { Component } from 'react';
import AddToCalendar from 'react-add-to-calendar';

function Calender(){
    const event= {
        title: 'Sample Event',
        description: 'This is the sample event provided as an example only',
        location: 'Portland, OR',
        startTime: '2016-09-16T20:15:00-04:00',
        endTime: '2016-09-16T21:45:00-04:00'
    }
    return 
    (
    <div>
        <h1>hello</h1>
        <AddToCalendar event={event}/>
    </div>
    ) ;
}
export default Calender;