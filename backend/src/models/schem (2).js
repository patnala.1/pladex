const mongoose =require("mongoose");

const bookschema =new mongoose.Schema({
    
    bookname:{
        type:String,
        required:true,
    },
    bookimglink:{
        type:String,
        required:true,
    },
    booklink:{
        type:String,
        required:true,
    },
    type:{
        type:String,
        required:true,
    },
    author:{
        type:String,
        required:true,
    },
    subject:{
        type:String,
        required:true,
    },
    course:{
        type:String,
        required:true,
    },
    branch:{
        type:String,
        required:true,
    },
    likes:{
        type:Number,
    },
    shares:{
        type:Number,
    },

})

const bookData=new mongoose.model("bookapi",bookschema);

module.exports = bookData;
