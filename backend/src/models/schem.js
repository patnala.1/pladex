const mongoose =require("mongoose");

const bookschema =new mongoose.Schema({
    
    Bookname:{
        type:String,
        
        
    },
    Bookimglink:{
        type:String,
       
    },
    Booklink:{
        type:String,
       
    },
    Authors:{
        type:String,
       
    },
    Subject:{
        type:String,
     
    },
    Branch:{
        type:String,
      
    },
    Likes:{
        type:Number,
    },
    Shares:{
        type:Number,
    },
    ShortForm: {
        type: String,
    }

})

const bookData=new mongoose.model("bookapi",bookschema);

module.exports = bookData;
