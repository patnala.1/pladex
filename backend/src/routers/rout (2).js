const express=require("express");
const router= new express.Router();
const bookData=require("../models/schem");

router.post("/bookapi",async (req,res)=>{
    try{
       const addingbook=new bookData(req.body);
       const receiveddata=await addingbook.save();
       res.status(201).send(receiveddata);
    }
    catch(e){
        res.status(400).send(e);
    }
})

router.get("/bookapi",async (req,res)=>{
    try{
      const getbook=await bookData.find({});
       res.status(201).send(getbook);
    }
    catch(e){
        res.status(400).send(e);
    }
})

module.exports=router;