const express=require("express");
const router= new express.Router();
const bookData=require("../models/schem");

router.post("/bookapi",async (req,res)=>{
    var newbook = {
        Bookname : req.body.Bookname ,
        Authors : req.body.Authors,
        Subject: req.body.Subject,
        Branch : req.body.Branch,
        Booklink:req.body.Booklink,
        Bookimglink : 'no',
        ShortForm : 'EC',
        Likes: 0,
        Shares : 0
    }
    console.log(req.body);
       const addingbook=new bookData(newbook);
      await addingbook.save()
       .then(item => {
        res.status(201).send("successfully uploaded");
       })
       .catch(err => {
           res.status(400).send(err);
           console.log(err);
       });

})

router.get("/bookapi",async (req,res)=>{
    try{
      const getbook=await bookData.find({});
       res.status(201).send(getbook);
    }
    catch(e){
        res.status(400).send(e);
    }
})

router.get("/bookapi/sort",async (req,res)=>{
    try{
      const getbook=await bookData.find().sort({Likes : -1});
       res.status(201).send(getbook);
    }
    catch(e){
        res.status(400).send(e);
    }
})

router.get("/bookapi/:search",async (req,res)=>{
    try{
        var reg= new RegExp(req.params.search, i)
      const getbook=await bookData.find({Subject: reg});
       res.status(201).send(getbook);
    }
    catch(e){
        res.status(400).send(e);
    }
})

module.exports=router;