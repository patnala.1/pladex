const express=require("express");
const app=express();
const port =process.env.PORT || 8000;
require("../src/db/conn");
const bookData =require("../src/models/schem");
const router = require("./routers/rout");

app.use(express.json())

app.use(router);


app.listen(port,(req,res)=>{
    console.log(`Connection established at port no ${port}`);
})